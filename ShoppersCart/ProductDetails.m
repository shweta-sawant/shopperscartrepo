//
//  ProductDetails.m
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import "ProductDetails.h"
#import "Products.h"


@implementation ProductDetails

@dynamic name;
@dynamic price;
@dynamic product;

@end
