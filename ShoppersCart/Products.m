//
//  Products.m
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import "Products.h"
#import "ProductDetails.h"
#import "UserCartItems.h"


@implementation Products

@dynamic productID;
@dynamic productDetails;
@dynamic userCartItem;

@end
