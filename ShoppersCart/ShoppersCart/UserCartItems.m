//
//  UserCartItems.m
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import "UserCartItems.h"
#import "Products.h"


@implementation UserCartItems

@dynamic numberOfItems;
@dynamic productAddedToCart;

@end
