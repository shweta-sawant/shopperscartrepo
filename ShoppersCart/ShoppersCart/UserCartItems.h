//
//  UserCartItems.h
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Products;

@interface UserCartItems : NSManagedObject

@property (nonatomic, retain) NSNumber * numberOfItems;
@property (nonatomic, retain) Products *productAddedToCart;

@end
