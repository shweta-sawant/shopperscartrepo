//
//  ProductDetailsViewController.m
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import "ProductDetailsViewController.h"

@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Button Actions

/*
 * Will add the item to users cart
 */
- (IBAction)addToCart:(id)sender {
}

/*
 * Will dissmiss the details page
 */
- (IBAction)closeDetailsPage:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
