//
//  main.m
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
