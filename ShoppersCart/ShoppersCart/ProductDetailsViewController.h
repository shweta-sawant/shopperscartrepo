//
//  ProductDetailsViewController.h
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailsViewController : UIViewController

- (IBAction)addToCart:(id)sender;
- (IBAction)closeDetailsPage:(id)sender;

@end
