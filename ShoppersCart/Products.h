//
//  Products.h
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProductDetails, UserCartItems;

@interface Products : NSManagedObject

@property (nonatomic, retain) NSNumber * productID;
@property (nonatomic, retain) ProductDetails *productDetails;
@property (nonatomic, retain) UserCartItems *userCartItem;

@end
