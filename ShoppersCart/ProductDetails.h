//
//  ProductDetails.h
//  ShoppersCart
//
//  Created by Shweta Sawant on 02/07/14.
//  Copyright (c) 2014 Shoppers Cart Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Products;

@interface ProductDetails : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) Products *product;

@end
